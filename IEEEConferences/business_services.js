Everlive.CloudFunction.onRequest(function(request, response, done) {
    
    //Get parameters from the request
    //var cn = request.queryString.cn;
    var ct = request.queryString.ct;
    var st = request.queryString.st;
   
    //Get the IEEE Conferences
    var url = "http://services28.ieee.org/wservice/rest/confsearch?ct="+ct;
    
    if(request.queryString.st !== undefined){
        url = url +"&st="+st;
    }   

    Everlive.Http.get(url, {}, function(error, httpResponse) {
        if (error) {
            //Always handle errors
            response.body = "Error while executing IEEE Conference service: " + error.message;
        } else {
            //Extract response on success
            response.body = httpResponse.data;
        }
        //Call done to end the execution of the Cloud Function
        done();
    });
});






Everlive.CloudFunction.onRequest(function(request, response, done) {
    
   
    //Get the User geo location
    var url = "http://freegeoip.net/json/";
    
   
    Everlive.Http.get(url, {}, function(error, httpResponse) {
        if (error) {
            //Always handle errors
            response.body = "Error while executing Geo location service: " + error.message;
        } else {
            //Extract response on success
            response.body = httpResponse.data;
        }
        //Call done to end the execution of the Cloud Function
        done();
    });
});
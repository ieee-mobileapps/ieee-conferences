'use strict';

var userLocationCode = "USA";
var userRegionCode = "";
var dataSource = new kendo.data.DataSource({
    transport: {
        read: {
            url: "https://platform.telerik.com/bs-api/v1/mkf4dcuq4ti0tba2/Functions/IEEEConferencesService?ct=" + userLocationCode + "&st=" + userRegionCode,
            // url: "http://services28.ieee.org/wservice/rest/confsearch?ct="+userLocationCode+"&st="+userRegionCode,
            dataType: "json"
        }
    },
    schema: {
        data: 'searchRecords'
    }
});

app.home = kendo.observable({
    onShow: function () {

        
        var items = [{
            text: "USA",
            value: "USA"
        }, {
            text: "India",
            value: "India"
        }, {
            text: "China",
            value: "China"
        }];
        $("#dropdownlist").kendoDropDownList({
            //autoBind: true,
            dataTextField: "text",
            dataValueField: "value",
            dataSource: items,
            unselectable:"off" 
            //optionLabel: "Change location..."
        }).data("kendoDropDownList").select(0);

        var dropdownlist = $("#dropdownlist").data("kendoDropDownList");
        dropdownlist.bind("change", reloadDatasource);
        $('.k-i-arrow-s').hide();

        // get user's location
        $.ajax({
            //	url: '//freegeoip.net/json/', 
            url: 'https://platform.telerik.com/bs-api/v1/mkf4dcuq4ti0tba2/Functions/UserGeoLocationService',
            type: 'GET',
            dataType: 'json',
            success: function (location) {
                // example where I update content on the page.

                if (location.country_code == "US") {
                    userLocationCode = "USA";
                    userRegionCode = location.region_code;
                } else {
                    userLocationCode = location.country_code;
                }
                $("#userlocationInfo").html(location.region_code + ", " + location.country_code);
                 dataSource.options.transport.read.url = "https://platform.telerik.com/bs-api/v1/mkf4dcuq4ti0tba2/Functions/IEEEConferencesService?ct=" + userLocationCode + "&st=" + userRegionCode
           //    dataSource.options.transport.read.url = "http://services28.ieee.org/wservice/rest/confsearch?ct="+userLocationCode+"&st="+userRegionCode;
                
                $("#products").kendoMobileListView({
                       dataSource: dataSource,
                       template: "<tr><td class='addborder'><label class='lblclass'>Start Date: </label><span style='padding-left:10px;'>#= convertToDate(startdate) #</span><br/><label class='lblclass'>End Date: </label><span style='padding-left:17px;'>#= convertToDate(enddate) #</span></br><span class='nameClass'>#= conferenceName #</span><br/><a id='showDetails' align='right' onClick='toggleDiv(this);'><img style='float:right;height:25px;width:25px' src='images/plusgreen.jpg' alt='Show Details'></a> <div style='display:none'><br/><label class='lblclass'>Conference Scope: </label><span>#= conferenceScope #</span><br/><br/><label class='lblclass'>Contact Info: </label><br/><span>#= contactInfo #</span><br/><label class='lblclass'>Url: </label><span>#= conferenceSiteURL #</span><br/><label class='lblclass'>Location: </label><span>#= city # , #= stateOrProvince # , #= country #</span></div></td></tr>"
            });

            }
        });


    },
    afterShow: function () {}
});



function toggleDiv(self) {
    if (self.nextElementSibling.getAttribute("style") == "display:none") {
        self.nextElementSibling.setAttribute("style", "display:block");
        self.innerHTML = "<img style='float:right;height:25px;width:25px' src='images/minusgreen.jpg' alt='Hide Details'>";
    } else {
        self.nextElementSibling.setAttribute("style", "display:none");
        self.innerHTML = "<img style='float:right;height:25px;width:25px' src='images/plusgreen.jpg' alt='Show Details'>";
    }
}



function changeLocationCode(self) {
    var locationCode = self.value;
}

function convertToDate(sysDate) {
    var dateToString = "";
    var options = {
        day: "numeric",
        month: "short",
        year: "numeric"
    };

    if (null != sysDate) {
        dateToString = new Date(parseInt(sysDate));
        return dateToString.toLocaleDateString("en-GB", options);
    }
    return "";
}

function reloadDatasource(e) {
    var userLocationCode = this.value();
   
     $("#userlocationInfo").html(userLocationCode);
    dataSource.options.transport.read.url = "https://platform.telerik.com/bs-api/v1/mkf4dcuq4ti0tba2/Functions/IEEEConferencesService?ct=" + userLocationCode;
    dataSource.read();
    $("#products").kendoMobileListView({
                       dataSource: dataSource,
                       template: "<tr><td class='addborder'><label class='lblclass'>Start Date: </label><span style='padding-left:10px;'>#= convertToDate(startdate) #</span><br/><label class='lblclass'>End Date: </label><span style='padding-left:17px;'>#= convertToDate(enddate) #</span></br><span class='nameClass'>#= conferenceName #</span><br/><a id='showDetails' align='right' onClick='toggleDiv(this);'><img style='float:right;height:25px;width:25px' src='images/plusgreen.jpg' alt='Show Details'></a> <div style='display:none'><br/><label class='lblclass'>Conference Scope: </label><span>#= conferenceScope #</span><br/><br/><label class='lblclass'>Contact Info: </label><br/><span>#= contactInfo #</span><br/><label class='lblclass'>Url: </label><span>#= conferenceSiteURL #</span><br/><label class='lblclass'>Location: </label><span>#= city # , #= stateOrProvince # , #= country #</span></div></td></tr>"
            });
}

// START_CUSTOM_CODE_home
// Add custom code here. For more information about custom code, see http://docs.telerik.com/platform/screenbuilder/troubleshooting/how-to-keep-custom-code-changes

// END_CUSTOM_CODE_home